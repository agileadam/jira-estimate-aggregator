# README #
This usercript shows the sum "Remaining Time" and "Σ Remaining Estimate" values for selected JIRA tasks in the Backlog screen.

This information appears at the top of the page and continues to show as you scroll through the tasks.

### Installation ###
Install this like you would any other browser userscript; in Chrome I prefer to use TamperMonkey.
Make sure the Σ in "Σ Remaining Estimate" doesn't get encoded strangely when you copy the js code!

### How do I get set up? ###

#### Board Configuration
For this to work you must setup the Backlog card layout to show the "Σ Remaining Estimate" value in the output of each item.

1. Open your board
2. Click the three dots at top right and go to "Board settings"
3. Go to "Card Layout"
4. Under "Backlog" add the "Σ Remaining Estimate" field

#### Time Display Format Configuration
Currently this is only setup to handle the "Hours (e.g. 36.5h)" time display format.

1. Open main Cogwheel » Issues
2. Click "Time tracking" in left sidebar
3. Click "Edit global settings"
4. Choose "Hours (e.g. 36.5h)" for the time display format

### Configuration
If you would like to change how frequently the time estimate is updated you can tweak the number of milliseconds at the very bottom of the script.