// ==UserScript==
// @name          JIRA Estimate Aggregator
// @namespace     http://agileadam.com
// @version       0.2
// @description   Shows the sum of remaining time estimates for selected JIRA tasks in the Backlog screen. Board card layout must include fields "Σ Remaining Estimate" and/or "Remaining Estimate". bitbucket.org/agileadam/jira-estimate-aggregator
// @match         https://*.atlassian.net/secure/RapidBoard.jspa*
// @require       http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js
// ==/UserScript==

(function() {
    'use strict';
    $(window).load(function() {
      $('.subnavigator-title').append('<span id="total-time-remaining" style="margin-left: 20px;background-color: #337ab7;padding: .1em .5em;vertical-align: middle;color: #ffffff;border-radius: .25em;font-size: 75%;line-height: 2;">0 mins</span>');
    });

    // Convert seconds to either "1:25 hours" or "1:25 mins"
    function secondsToString(seconds) {
        //var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
        var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
        var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;

        var output = '';

        // We want only hours and minutes even if more than 24 hours (so no days, per se)
        if (numdays > 0) {
            numhours += numdays * 24;
        }

        if (numhours > 0) {
            if (numminutes > 0) {
                output += numhours + ':' + (numminutes < 10 ? '0' : '') + numminutes + ' hours';
            }
            else {
                output += numhours + ' hours';
            }
        }
        else {
            if (numseconds > 0) {
                output += numminutes + ':' + (numseconds < 10 ? '0' : '') + numseconds + ' mins';
            }
            else {
                output += numminutes + ' mins';
            }
        }

        return output;
    }

    function everyXSecs() {
        var total = 0;
        var re = /([0-9.]+)h ([0-9.]+)m/i;
        $('.js-issue.ghx-selected').each(function() {
            // First, look for a sum estimate value
            var estimate = $(this).find('.ghx-extra-field[data-tooltip^="Σ Remaining Estimate"] .ghx-extra-field-content').text();

            // If no sum estimate, look for a single-item estimate
            if (estimate === "") {
                estimate = $(this).find('.ghx-extra-field[data-tooltip^="Remaining Estimate"] .ghx-extra-field-content').text();
            }

            // If we don't have any estimates, continue to the next item
            if (estimate === "") {
                return true;
            }

            // Process the result
            var found = estimate.match(re);
            if (found !== null && found[1] && found[2]) {
                // This is a value like "3h 55m"
                total += found[1] * 60 + found[2] * 1;
            }
            else if (estimate.endsWith('m')) {
                // Just minutes
                total += estimate.slice(0, -1) * 1;
            }
            else if (estimate.endsWith('h')) {
                // Just hours; convert to minutes
                total += estimate.slice(0, -1) * 60;
            }
        });

        // Update the remaining time
        $('#total-time-remaining').html(secondsToString(total * 60));
    }

    // Repeat some things every X seconds
    var everyXSecsInterval = setInterval(everyXSecs, 4000);
})();